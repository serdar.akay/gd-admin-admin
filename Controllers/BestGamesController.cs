﻿using GameDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GameDistribution.Controllers
{
    [Authorize(Roles = "Superadmin,Admin")]
    public class BestGamesController : Controller
    {
        GameDistributionEntitiesDataContext context = new GameDistributionEntitiesDataContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RemoveBest(int id)
        {
            var result = context.ADM_RemoveBestGame(id);
            return RedirectToAction("Index", "BestGames");
        }

        public JsonResult GetBestGames(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            var query = Request.QueryString;
            var item = context.ExecuteQuery<view_ADM_BestGame>(Utils.Utils.BuildQuery(query, "select * from view_ADM_BestGames")).OrderByDescending(g => g.AddedOn).ToList();
            IEnumerable<view_ADM_BestGame> bestGames = from bestGame in item
                                                       select new view_ADM_BestGame
                                                       {
                                                           Id = bestGame.Id,
                                                           Game = bestGame.Game,
                                                           GameMd5 = bestGame.GameMd5,
                                                           Warning = bestGame.Warning,
                                                           Visible = bestGame.Visible,
                                                           Banner = bestGame.Banner,
                                                           Thumbnail = bestGame.Thumbnail,
                                                           AddedOn = bestGame.AddedOn,
                                                           Company = bestGame.Company,
                                                           Order = bestGame.Order,
                                                           CompanyOrder = bestGame.CompanyOrder
                                                       };
            var total = item.Count();
            bestGames = bestGames.Skip(pagesize * pagenum).Take(pagesize);
            if (sortdatafield != null && sortorder != "")
            {
                bestGames = Utils.Utils.SortOrders<view_ADM_BestGame>(bestGames, sortdatafield, sortorder);
            }
            var result = new
            {
                TotalRows = total,
                Rows = bestGames
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult UpdateBestGames(GetBestGamesResult result)
        {
            context.UpdateBestGame(result.Id.ToString(),
                                    result.Title,
                                    result.GameMd5,
                                    Convert.ToBoolean(result.Approved),
                                    Convert.ToBoolean(result.Visible),
                                    Convert.ToBoolean(result.Banner),
                                    result.Warning,
                                    Convert.ToBoolean(result.Analytics),
                                    result.Order,
                                    result.CompanyOrder);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}