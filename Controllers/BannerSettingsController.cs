﻿using GameDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GameDistribution.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class BannerSettingsController : Controller
    {
        GameDistributionEntitiesDataContext context = new GameDistributionEntitiesDataContext();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            var query = Request.QueryString;
            var items = context.ExecuteQuery<view_BannerSetting>(Utils.Utils.BuildQuery(query, "select * from view_BannerSettings")).OrderByDescending(g => g.AddedDate).ToList();
            IEnumerable<view_BannerSetting> bannerSettings = from banner in items
                                                             select new view_BannerSetting
                                                             {
                                                                 Id = banner.Id,
                                                                 UserId =banner.UserId,
                                                                 Title = banner.Title,
                                                                 GameMd5 = banner.GameMd5,
                                                                 Domain = banner.Domain,
                                                                 CompanyName = banner.CompanyName,
                                                                 AddedDate = banner.AddedDate,
                                                                 BannerEnable = banner.BannerEnable,
                                                                 PreRoll = banner.PreRoll,
                                                                 ShowAfterTime = banner.ShowAfterTime,
                                                                 Deleted = banner.Deleted
                                                             };

            var total = items.Count();
            bannerSettings = bannerSettings.Skip(pagesize * pagenum).Take(pagesize);
            if (sortdatafield != null && sortorder != "")
            {
                bannerSettings = Utils.Utils.SortOrders<view_BannerSetting>(bannerSettings, sortdatafield, sortorder);
            }
            var result = new
            {
                TotalRows = total,
                Rows = bannerSettings
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var item = context.ADM_GetBannerSettingByGameId(id).FirstOrDefault();
            var domainView = new DomainViewModel();

            domainView.GameId = item.GameMd5;
            domainView.Title = item.Title;
            domainView.PreRoll = item.PreRoll;
            domainView.BannerEnable = item.BannerEnable;
            domainView.ShowAfterTime = item.ShowAfterTime;

            return View(domainView);
        }


        [HttpPost]
        public ActionResult Update(DomainViewModel domainView)
        {
            context.ADM_UpdateBannerByGameId(domainView.GameId,
                                          domainView.BannerEnable == null ? false : domainView.BannerEnable,
                                          domainView.PreRoll == null ? false : domainView.PreRoll,
                                          domainView.ShowAfterTime == null ? 0 : domainView.ShowAfterTime);

            return View("index");
        }
    }
}