﻿using GameDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using System.Xml.Linq;

namespace GameDistribution.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class WhitelistDomainsController : Controller
    {

        GameDistributionEntitiesDataContext context = new GameDistributionEntitiesDataContext();

        // GET: WhitelistDomains
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UnPublishedDomains(string id)
        {
            try
            {
                ViewBag.UnPublishedGameId = id;
                ViewBag.Game = context.GetGameDetailByMd5Id(id).FirstOrDefault().Title;

                return View();
            }
            catch
            {
                return RedirectToAction("NotFound", "Error");
            }

        }

        public ActionResult UnPublishedGames(string id)
        {
            return RedirectToAction("UnPublishedGames", "Games", new { id = id });
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            //VGD-144
            var item = context.ADM_GetDomainByDomainId(id).FirstOrDefault();

            if (item == null)
                return null;

            var viewResult = new DomainViewModel();

            viewResult.Domain = item.Domain;
            viewResult.PreRoll = item.PreRoll;
            viewResult.BannerEnable = item.BannerEnable;
            viewResult.ShowAfterTime = item.ShowAfterTime;

            return View(viewResult);
        }

        [HttpPost]
        public ActionResult UpdateDomain(DomainViewModel domainView)
        {
            // VGD - 144
            //context.ADM_UpdateBannersByDomain(Utils.Utils.TrimDomain(domainView.Domain),
            //                                 domainView.BannerEnable == null ? false : domainView.BannerEnable,
            //                                 domainView.PreRoll == null ? false : domainView.PreRoll,
            //                                 domainView.ShowAfterTime == null ? 0 : domainView.ShowAfterTime);

            context.ADM_UpdateBannersByDomain(domainView.Id,
                                          domainView.BannerEnable == null ? false : domainView.BannerEnable,
                                          domainView.PreRoll == null ? false : domainView.PreRoll,
                                          domainView.ShowAfterTime == null ? 0 : domainView.ShowAfterTime);

            //going to be deleted at vgd-144
            //context.ADM_UpdateDomainNameById(domainView.Id,domainView.Domain);

            return View("index");
        }

        public ActionResult Delete(int id)
        {
            var item = context.ADM_Delete_WhiteListDomain(id);
            return View("Index");
        }

        public JsonResult GetWhiteListDomains(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            var query = Request.QueryString;
            var item = context.ExecuteQuery<view_WhitelistDomain>(Utils.Utils.BuildQuery(query, "select * from view_WhitelistDomains")).OrderByDescending(g => g.AddedDate).ToList();
            IEnumerable<view_WhitelistDomain> whiteListDomains = from domain in item
                                                                 select new view_WhitelistDomain
                                                                 {
                                                                     Id = domain.Id,
                                                                     AdNetworkId = domain.AdNetworkId,
                                                                     Domain = domain.Domain,
                                                                     AddedDate = domain.AddedDate,
                                                                     HtmlBanner = domain.HtmlBanner,
                                                                     FamobiId = domain.FamobiId,
                                                                     Active = domain.Active,
                                                                     SyncTunnl = domain.SyncTunnl
                                                                 };
            var total = item.Count();
            whiteListDomains = whiteListDomains.Skip(pagesize * pagenum).Take(pagesize);
            if (sortdatafield != null && sortorder != "")
            {
                whiteListDomains = Utils.Utils.SortOrders<view_WhitelistDomain>(whiteListDomains, sortdatafield, sortorder);
            }
            var result = new
            {
                TotalRows = total,
                Rows = whiteListDomains
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult UpdateDomainsInline(view_WhitelistDomain result)
        {
            context.ADM_UpdateWhitelistDomains(result.Id,
                                              //result.Domain,
                                              result.HtmlBanner,
                                              int.Parse(result.Active.ToString()),
                                              result.FamobiId);

            return Json(true, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AddDomains(string data)
        {
            string[] domains = data.Split('\n');
            XDocument xml = new XDocument(new XElement("Domains"));

            foreach (var item in domains)
            {
                xml.Root.Add(new XElement("Row", new XElement("Domain", item)));
                CreateDomainApi(item);
            }

            context.APP_WhiteListDomainAdd(1, xml.Root);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUnPublishedDomains(string id, string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            try
            {
                var query = Request.QueryString;
                string filterValue = Request.QueryString["filtervalue0"];
                string filterField = Request.QueryString["filterdatafield0"];

                IEnumerable<ADM_GetUnPublishedDomainsByGameMD5Result> data = context.ADM_GetUnPublishedDomainsByGameMD5(id).OrderByDescending(x => x.CreatedDate).ToList();
                if (!string.IsNullOrEmpty(filterValue))
                {
                    if (filterField == "AffiliateId")
                    {
                        data = data.Where(x => x.AffiliateId.Contains(filterValue));
                    }
                    else if (filterField == "Domain")
                    {
                        data = data.Where(x => x.Domain.Contains(filterValue));
                    }
                }
                var total = data.Count();
                data = data.Skip(pagesize * pagenum).Take(pagesize).ToList();


                var result = new
                {
                    TotalRows = total,
                    Rows = data
                };


                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public void CreateDomainApi(string url)
        {
            try
            {
                using (var http = new System.Net.Http.HttpClient())
                {
                    //Create Partner
                    string createGameTagRequest = @"{
                                                    ""Session"":{
                                                    ""Token"":""1356b0be-8156-4dae-97d4-4d573d6c3b88""
                                                    },
                                                    ""Data"":{
                                                    ""Url"":'" + url + @"',
                                                    ""AdTagType"":'" + 1 + @"',
                                                    }
                                                }";
                    var content = new StringContent(createGameTagRequest);
                    content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

                    var request = http.PostAsync("http://api.tunnl.com:8080/Platform/CreatePlatform", content);
                    var response = request.Result.Content.ReadAsStringAsync().Result;
                    InsertLog(url, response);
                }
            }
            catch
            {
                //string deneme = ex.Message;
            }

        }

        public void InsertLog(string url, string jsonString)
        {
            using (GameDistributionEntitiesDataContext context = new GameDistributionEntitiesDataContext())
            {
                TunnlLogDataModel log = Newtonsoft.Json.JsonConvert.DeserializeObject<TunnlLogDataModel>(jsonString);
                context.AddTunnlLog(2, url, log.ResponseCode, log.ResponseText, log.IsValid == true ? log.Data.Id : 0, log.IsValid);
            }
        }
    }
}