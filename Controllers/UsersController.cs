﻿using GameDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GameDistribution.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class UsersController : Controller
    {
        GameDistributionEntitiesDataContext context = new GameDistributionEntitiesDataContext();

        public ViewResult Index(int? id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Games(string id)
        {
            ViewBag.Id = id;
            return View("UserGames");
        }

        public JsonResult GetGameById(string sortdatafield, string sortorder, int pagesize, int pagenum, string id)
        {
            var query = Request.QueryString;
            var item = context.ExecuteQuery<view_ADM_Game>(Utils.Utils.BuildQuery(query, "select * from view_ADM_Games")).Where(a => a.UserId == int.Parse(id)).OrderByDescending(g => g.AddedOn).ToList();
            IEnumerable<view_ADM_Game> games = from game in item
                                               select new view_ADM_Game
                                               {
                                                   Id = game.Id,
                                                   GameId = game.GameId,
                                                   GameMd5 = game.GameId,
                                                   Game = game.Game,
                                                   Warning = game.Warning,
                                                   Visible = game.Visible,
                                                   Banner = game.Banner,
                                                   Thumbnail = game.Thumbnail,
                                                   AddedOn = game.AddedOn,
                                                   Company = game.Company,
                                                   UserId = game.UserId
                                               };
            var total = item.Count();
            games = games.Skip(pagesize * pagenum).Take(pagesize);
            if (sortdatafield != null && sortorder != "")
            {
                games = Utils.Utils.SortOrders<view_ADM_Game>(games, sortdatafield, sortorder);
            }
            var result = new
            {
                TotalRows = total,
                Rows = games
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult EditGame(string id)
        {
            var item = context.RPT_GamesById_Admin(id).FirstOrDefault();

            ViewBag.Category = new SelectList(context.GetCategories(), "Id", "Title", item.CategoryId);
            ViewBag.Company = new SelectList(context.STAT_getUsersList("s1").OrderBy(u => u.CompanyName), "Id", "CompanyName", item.UserId);
            return View(item);
        }

        [HttpGet]
        public ViewResult CreateUser()
        {

            IEnumerable<SelectListItem> roles = context.ADM_GetAdminRoles().Select(r => new SelectListItem
            {
                Value = r.RoleId.ToString(),
                Text = r.RoleName
            }).ToList();

            ViewBag.Roles = roles;

            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(UserViewModel user)
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                if (user.Password != user.ConfirmPassword)
                {
                    ViewBag.message = "Password not match";
                    return View("Index");
                }


                var item = from u in context.ADM_RegisterUser(user.Password, user.Firstname, user.Lastname, user.Email, user.CompanyName, user.Website, user.Active, user.userType, null) select u;
                var result = item.ToList();
                if (result.FirstOrDefault().Result == "Exists")
                {
                    ViewBag.message = "User already exists";
                }
                else
                {
                    if (user.SendEmail == true)
                    {
                        string mailbody = "Hello {0} {1},<br /><br />";
                        mailbody += "Here is your account activation code <a href=\"http://admin.gamedistribution.com/Users/ActivateUser?regid={2}&email={3}\" target=\"_blank\">{2}</a>, please click for activating your account.<br><br>Thank you,<br>GameDistribution<br>&copy; 2010-2017";
                        mailbody = String.Format(mailbody, Utils.Utils.safeStr(user.Firstname), Utils.Utils.safeStr(user.Lastname), result.FirstOrDefault().Result, Utils.Utils.safeStr(user.Email));
                        Utils.Utils.gmailEmailServer(user.Email, "GameDistribution Registration <gd@GameDistribution.com>", "Account Activation", mailbody);
                    }
                    if (user.Active == true && Utils.Utils.validateEmail(Utils.Utils.safeStr(user.Email)))
                    {
                        var results = context.ADM_ActivateRegisterUser(result.FirstOrDefault().Result, user.Email).ToList();
                    }

                    ViewBag.message = "User registered succesfully";
                }
                return View("Index");
            }
            else
            {
                return View("CreateUser");
            }
        }

        public ActionResult ActivateUser(string regid, string email)
        {
            if (Utils.Utils.validateEmail(Utils.Utils.safeStr(email)))
            {
                try
                {
                    var userResults = context.ADM_ActivateRegisterUser(regid, email).ToList();

                    ViewBag.message = userResults.FirstOrDefault().Result;
                    return View("ActivateUser");
                }
                catch
                {
                    return View("ActivateUser");
                }
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Update(int? id)
        {
            var item = context.ADM_UsersById(id).FirstOrDefault();
            var userView = new UserViewModel();

            userView.Id = item.Id;
            userView.Firstname = item.Name;
            userView.Lastname = item.Lastname;
            userView.Email = item.Email;
            userView.CompanyName = item.CompanyName;
            userView.Website = item.WebsiteURL;
            userView.Active = item.Active;
            userView.PartnerId = item.PartnerId;
            userView.DeveloperId = item.DeveloperId;
            userView.UserId = item.UserId;
            userView.userType = item.UserType;
            userView.userTypeId = item.UserType.ToString();


            return View("EditUser", userView);
        }

        [HttpPost]
        public ActionResult UpdateUsers(UserViewModel result)
        {
            if (result.Active == true)
            {
                var userResults = context.ActivateRegisterUser(result.RegId, result.Email).ToList();
                ViewBag.message = userResults.FirstOrDefault().Result;
            }

            result.Password = result.Password == null ? string.Empty : result.Password.Trim();

            context.ADM_UpdateUsers(result.Id,
                                    result.Firstname,
                                    result.Lastname,
                                    result.Email,
                                    result.Password,
                                    result.Website,
                                    result.CompanyName,
                                    Convert.ToBoolean(result.Active),
                                    int.Parse(result.userTypeId),
                                    result.PartnerId,
                                    result.DeveloperId,
                                    result.UserId);

            return RedirectToAction("Index");
        }

        public ActionResult ActivateAccount()
        {
            string email = Request.QueryString["email"];
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                var result = db.ADM_ActivateAdmins(email).FirstOrDefault().Column1.ToString();

                if (result == "Active")
                {
                    ViewBag.Activation = "Account was already activated";
                    return View("Login");
                }
                else if (result == "Updated")
                {
                    ViewBag.Activation = "Account activated";
                    return View("Login");
                }
                else
                {
                    ViewBag.Activation = "Account not found";
                    return View("Login");
                }
            }
        }

        public JsonResult UpdateUserGamesInline(RPT_GamesById_AdminResult result)
        {
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                db.UpdateGame(null, result.GameMd5, null, Convert.ToBoolean(result.Approved), null, Convert.ToBoolean(result.Banner),
                    null, null, null, null, null, null, null, null, null, null, null, Convert.ToBoolean(result.Visible));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
    }
}