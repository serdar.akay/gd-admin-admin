﻿using GameDistribution.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using static GameDistribution.IdentityConfig;

namespace GameDistribution.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext();
            var user = db.GetAdmins().Where(u => u.Email == model.Username && u.Password == model.Password).FirstOrDefault();

            if (user != null)
            {
                FormsAuthentication.SetAuthCookie(model.Username, false);

                var authTicket = new FormsAuthenticationTicket(1, user.Email, DateTime.Now, DateTime.Now.AddMinutes(720), false, user.RoleName);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Response.Cookies.Add(authCookie);
                return RedirectToAction("Index", "Home");
            }

            else
            {
                ViewBag.Error = "Wrong Email or Password!!";
                ModelState.AddModelError("", "Invalid credentials");
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (Utils.Utils.validateEmail(Utils.Utils.safeStr(model.Email)))
            {
                ViewBag.message = "";
                try
                {
                    GameDistributionEntitiesDataContext context = new GameDistributionEntitiesDataContext();
                    var results = context.LostPasswordAdmin(model.Email).FirstOrDefault();

                    if (results?.Result != "NotExists")
                    {
                        string mailbody = "Hello {0} {1},<br /><br />";
                        mailbody += "Your password : {2} <br /> <a href='http://admin.gamedistribution.com/Account/Login' target='_blank'>Login</a><br><br>Thank you,<br>GameDistribution<br>&copy; 2011-2017";
                        mailbody = String.Format(mailbody, Utils.Utils.safeStr(results.Name), Utils.Utils.safeStr(results.Lastname), Utils.Utils.safeStr(results.Password));
                        Utils.Utils.gmailEmailServer(Utils.Utils.safeStr(results.Email), "GameDistribution Lostpassword <gd@GameDistribution.com>", "Password Notification", mailbody);

                        ViewBag.message = "Email has been sent";
                        return View("ForgotPassword");
                    }
                    else
                    {
                        ViewBag.message = "User Not Found";
                        return View("ForgotPassword");
                    }
                }
                catch
                {
                    return View("~/Views/Login/ActivateUser");
                }
            }
            else
            {
                return View();
            }
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [Authorize(Roles = "SuperAdmin")]
        public ViewResult AdminUsers()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                var result = db.ADM_AdminsById(id).FirstOrDefault();
                ViewBag.Roles = new SelectList(db.ADM_GetAdminRoles().ToList(), "RoleId", "RoleName", result.RoleId);
                return View(result);
            }
        }

        [HttpPost]
        public ActionResult Edit(ADM_AdminsByIdResult result)
        {
            GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext();
            db.ADM_UpdateAdmins(
                result.Id,
                result.Name,
                result.Lastname,
                result.Email,
                result.Active,
                Convert.ToInt32(result.Roles)
            );
            return RedirectToAction("AdminUsers");
        }

        public JsonResult Delete(int id)
        {
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                var result = db.ADM_DeleteAdmin(id);
                var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
                return jsonResult;
            }
        }

        public JsonResult GetAdminRoles()
        {
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                var result = db.ADM_GetAdminRoles().ToList();
                var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
                return jsonResult;
            }
        }

        public JsonResult GetAdmins(int pagesize, int pagenum)
        {
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                var filtersCount = int.Parse(Request.QueryString.GetValues("filterscount")[0]);
                if (filtersCount > 0)
                {
                    for (int i = 0; i < filtersCount; i++)
                    {
                        var filterValue = Request.QueryString.GetValues("filtervalue" + i)[0];
                        var filterCondition = Request.QueryString.GetValues("filtercondition" + i)[0];
                        var filterDataField = Request.QueryString.GetValues("filterdatafield" + i)[0];
                        var filterOperator = Request.QueryString.GetValues("filteroperator" + i)[0];
                        int? totalAdmins = 0;
                        var result = db.ADM_GetAdmins(pagesize, pagenum, filterDataField, filterValue, "Id", "", ref totalAdmins).ToList();

                        var finalresult = new
                        {
                            TotalRows = result.Count(),
                            Rows = result.Skip(pagesize * pagenum).Take(pagesize)
                        };
                        var jsonResult = Json(finalresult, JsonRequestBehavior.AllowGet);
                        jsonResult.MaxJsonLength = int.MaxValue;
                        return jsonResult;
                    }
                    var finalresult1 = new
                    {
                        TotalRows = 12,
                        Rows = 5
                    };
                    var jsonResult1 = Json(finalresult1, JsonRequestBehavior.AllowGet);
                    jsonResult1.MaxJsonLength = int.MaxValue;
                    return jsonResult1;
                }
                else
                {
                    int? totalAdmins = 0;
                    var result = db.ADM_GetAdmins(pagesize, pagenum, "", "", "title", "", ref totalAdmins).ToList();
                    var finalresult = new
                    {
                        TotalRows = totalAdmins,
                        Rows = result
                    };
                    var jsonResult = Json(finalresult, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
            }
        }

        public JsonResult GetUsers(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                var query = Request.QueryString;
                var dbResult = db.ExecuteQuery<view_ADM_User>(Utils.Utils.BuildQuery(query, "select * from view_ADM_Users")).OrderByDescending(u => u.RegDate).ToList();
                IEnumerable<view_ADM_User> users = from user in dbResult
                                                   select new view_ADM_User
                                                   {
                                                       Id = user.Id,
                                                       //Password = user.Password,
                                                       Name = user.Name,
                                                       Lastname = user.Lastname,
                                                       Email = user.Email,
                                                       CompanyName = user.CompanyName,
                                                       WebsiteURL = user.WebsiteURL,
                                                       ServerName = user.ServerName,
                                                       Active = user.Active,
                                                       LastLoginIP = user.LastLoginIP,
                                                       UserType = user.UserType,
                                                       RegDate = user.RegDate,
                                                       SyncTunnl =user.SyncTunnl
                                                   };
                var total = dbResult.Count();
                users = users.Skip(pagesize * pagenum).Take(pagesize);
                var result = new
                {
                    TotalRows = total,
                    Rows = users
                };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ViewResult CreateAdmin()
        {
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                IEnumerable<SelectListItem> roles = db.ADM_GetAdminRoles().Select(r => new SelectListItem
                {
                    Value = r.RoleId.ToString(),
                    Text = r.RoleName
                }).ToList();

                ViewBag.Roles = roles;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateAdmin(AdminViewModel result)
        {
            if (ModelState.IsValid)
            {
                using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
                {
                    db.ADM_CreateAdmins(result.Email, result.Name, result.Lastname, result.RoleId, false);
                    var password = db.GetUsers().ToList().Where(u => u.Email == result.Email).FirstOrDefault().Password;
                    string mailbody = "Hello {0} {1},<br /><br />";
                    mailbody += "{2} added you as a user<br />";
                    mailbody += "Your password: {3} <br /> <a href='http://admin.gamedistribution.com/Account/ActivateAccount/?email=" + result.Email + "' target='_blank'>Activate your account</a><br><br>Thank you,<br />GameDistribution<br />&copy; 2011-2017";
                    mailbody = String.Format(mailbody, Utils.Utils.safeStr(result.Name), Utils.Utils.safeStr(result.Lastname), User.Identity.GetUserName(), Utils.Utils.safeStr(password), Utils.Utils.safeStr(result.Password));
                    Utils.Utils.gmailEmailServer(Utils.Utils.safeStr(result.Email), "GameDistribution Activation <gd@GameDistribution.com>", "Password Notification", mailbody);
                }
                return RedirectToAction("AdminUsers");
            }
            else
            {
                return View("CreateAdmin");
            }
        }

        public ActionResult ActivateAccount()
        {
            string email = Request.QueryString["email"];
            using (GameDistributionEntitiesDataContext db = new GameDistributionEntitiesDataContext())
            {
                var result = db.ADM_ActivateAdmins(email).FirstOrDefault().Column1.ToString();

                if (result == "Active")
                {
                    ViewBag.Activation = "Account was already activated";
                    return View("Login");
                }
                else if (result == "Updated")
                {
                    ViewBag.Activation = "Account activated";
                    return View("Login");
                }
                else
                {
                    ViewBag.Activation = "Account not found";
                    return View("Login");
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion




    }
}