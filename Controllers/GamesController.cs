﻿using GameDistribution.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace GameDistribution.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class GamesController : Controller
    {

        GameDistributionEntitiesDataContext context = new GameDistributionEntitiesDataContext();

        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        public ActionResult UnPublishedGames(string id)
        {
            try
            {
                ViewBag.UnPublishedAffiliateId = id;
                ViewBag.Domain = context.view_WhitelistDomains.Where(x => x.FamobiId == id).FirstOrDefault().Domain;
                return View();
            }
            catch
            {
                return RedirectToAction("NotFound", "Error");
            }
        }
        
        [HttpGet]
        public ActionResult Edit(string Id)
        {
            try
            {
                var item = context.ADM_GamesByGameId(Id).FirstOrDefault();

                ViewBag.Category = new SelectList(context.GetCategories(), "Id", "Title", item.CategoryId);
                ViewBag.Company = new SelectList(context.STAT_getUsersList("s1").OrderBy(u => u.CompanyName), "Id", "CompanyName", item.UserId);

                return View(item);
            }
            catch (Exception)
            {
                return RedirectToAction("NotFound", "Error");
            }

        }

        public JsonResult UpdateGamesInline(view_ADM_Game result)
        {

            context.UpdateGame(null, result.GameMd5, null, null, null, null,
                    null, null, null, null, null, null, null, null, null, null, null, Convert.ToBoolean(result.Visible)
                );
            return Json(true, JsonRequestBehavior.AllowGet);

        }

        public JsonResult UpdateRequest(string id, string requestId, string userId, string title, string condition)
        {
            var mail = string.Empty;

            context.ADM_UpdateRequest(int.Parse(id), int.Parse(requestId));
            mail = context.ADM_UsersById(int.Parse(userId)).FirstOrDefault().Email;

            if (!string.IsNullOrEmpty(condition))
            {
                var mailResult = SendEmail(mail, title, condition);
            }

            return Json(requestId, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Update(ADM_GamesByGameIdResult result)
        {
            context.ADM_UpdateGame(result.UserId,
                                result.GameMd5,
                                result.Title,
                                result.GameType,
                                result.Width,
                                result.Height,
                                result.ExternalURL,
                                result.Description,
                                result.Instructions,
                                result.Filename,
                                Convert.ToInt32(result.Category),
                                Convert.ToBoolean(result.Visible));

            context.ADM_MyGameBundleAdd(result.Id, 
                                    result.AndroidBundleId, 
                                    result.IOSBundleId);

            //VGD-144
            context.ADM_UpdateBannerByGameId(result.GameMd5,
                                        result.BannerEnable == null ? false : result.BannerEnable,
                                        result.PreRoll == null ? false : result.PreRoll,
                                        result.ShowAfterTime == null ? 0 : result.ShowAfterTime);

            return RedirectToAction("Index");
        }

        public JsonResult Get(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {

            var query = Request.QueryString;
            var item = context.ExecuteQuery<view_ADM_Game>(Utils.Utils.BuildQuery(query, "SELECT * FROM view_ADM_Games")).OrderByDescending(g => g.AddedOn).ToList();
            IEnumerable<view_ADM_Game> games = from game in item
                                               select new view_ADM_Game
                                               {
                                                   Id = game.Id,
                                                   GameId = game.GameId,
                                                   Game = game.Game,
                                                   Warning = game.Warning,
                                                   Visible = game.Visible,
                                                   Banner = game.Banner,
                                                   Thumbnail = game.Thumbnail,
                                                   AddedOn = game.AddedOn,
                                                   Company = game.Company,
                                                   Api = game.Api,
                                                   UserId = game.UserId,
                                                   GameType = game.GameType,
                                                   TypeName = game.TypeName,
                                                   Active = game.Active,
                                                   Deleted = game.Deleted,
                                                   RequestId = game.RequestId,
                                                   Request = game.Request,
                                                   SyncTunnl = game.SyncTunnl
                                               };
            var total = item.Count();
            games = games.Skip(pagesize * pagenum).Take(pagesize);
            if (sortdatafield != null && sortorder != "")
            {
                games = Utils.Utils.SortOrders<view_ADM_Game>(games, sortdatafield, sortorder);
            }
            var result = new
            {
                TotalRows = total,
                Rows = games
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetUnPublishedGames(string id, string sortdatafield, string sortorder, int pagesize, int pagenum)
        {
            try
            {
                var query = Request.QueryString;
                string filterValue = Request.QueryString["filtervalue0"];
                string filterField = Request.QueryString["filterdatafield0"];

                IEnumerable<ADM_GetUnPublishedGamesByPortalMD5Result> data = context.ADM_GetUnPublishedGamesByPortalMD5(id).OrderByDescending(x => x.AddedDate).ToList();
                if (!string.IsNullOrEmpty(filterValue))
                {
                    if (filterField == "Title")
                    {
                        data = data.Where(x => x.Title.Contains(filterValue));
                    }
                    else if (filterField == "GameMd5")
                    {
                        data = data.Where(x => x.GameMd5.Contains(filterValue));
                    }
                }
                var total = data.Count();
                data = data.Skip(pagesize * pagenum).Take(pagesize).ToList();


                var result = new
                {
                    TotalRows = total,
                    Rows = data
                };


                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Feature(int id)
        {

            var result = context.APP_InsertFeaturedGames(id.ToString());
            return RedirectToAction("Index", "FeaturedGames");
        }

        public ActionResult Best(int id)
        {
            var result = context.APP_InsertBestGames(id.ToString());
            return RedirectToAction("Index", "BestGames");
        }

        public ActionResult UnPublishedDomains(string id)
        {

            return RedirectToAction("UnPublishedDomains", "WhitelistDomains", new { id = id });
        }

        private bool SendEmail(string mail, string title, string condition)
        {
            try
            {
                var htmlBodyPath = string.Empty;
                switch (condition)
                {
                    case "publish-approved":
                        htmlBodyPath = @"~\Utils\htmlBody\request-publish-approved.html";
                        break;
                    case "publish-declined":
                        htmlBodyPath = @"~\Utils\htmlBody\request-publish-declined.html";
                        break;
                    case "deactivate-declined":
                        htmlBodyPath = @"~\Utils\htmlBody\request-deactivate-declined.html";
                        break;
                    case "deactivate-approved":
                        htmlBodyPath = @"~\Utils\htmlBody\request-deactivate-approved.html";
                        break;
                }

                using (StreamReader reader = System.IO.File.OpenText(Server.MapPath(htmlBodyPath))) // Path to your 
                {
                    string htmlBody = reader.ReadToEnd().Replace("{{gameName}}", title);
                    Utils.Utils.gmailEmailServer(Utils.Utils.safeStr(mail), "Game Distribution <support@gamedistribution.com> ", "Game Distribution Response", htmlBody);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}