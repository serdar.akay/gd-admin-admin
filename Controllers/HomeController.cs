﻿using System.Web.Mvc;

namespace GameDistribution.Controllers
{
    [Authorize(Roles = "Superadmin,Admin")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Games");
        }
    }
}