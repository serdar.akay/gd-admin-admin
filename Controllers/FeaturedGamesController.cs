﻿using GameDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GameDistribution.Controllers
{
    [Authorize(Roles = "Superadmin,Admin")]
    public class FeaturedGamesController : Controller
    {
        GameDistributionEntitiesDataContext context = new GameDistributionEntitiesDataContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RemoveFeature(int id)
        {

            var result = context.ADM_RemoveFeaturedGame(id);
            return RedirectToAction("Index", "FeaturedGames");
        }

        public JsonResult GetFeaturedGames(string sortdatafield, string sortorder, int pagesize, int pagenum)
        {

            var query = Request.QueryString;
            var item = context.ExecuteQuery<view_ADM_FeaturedGame>(Utils.Utils.BuildQuery(query, "select * from view_ADM_FeaturedGames")).OrderByDescending(g => g.AddedOn).ToList();
            IEnumerable<view_ADM_FeaturedGame> featuredGames = from featuredGame in item
                                                               select new view_ADM_FeaturedGame
                                                               {
                                                                   Id = featuredGame.Id,
                                                                   Game = featuredGame.Game,
                                                                   GameMd5 = featuredGame.GameMd5,
                                                                   Warning = featuredGame.Warning,
                                                                   Visible = featuredGame.Visible,
                                                                   Banner = featuredGame.Banner,
                                                                   Thumbnail = featuredGame.Thumbnail,
                                                                   AddedOn = featuredGame.AddedOn,
                                                                   Company = featuredGame.Company,
                                                                   Order = featuredGame.Order,
                                                                   CompanyOrder = featuredGame.CompanyOrder
                                                               };
            var total = item.Count();
            featuredGames = featuredGames.Skip(pagesize * pagenum).Take(pagesize);
            if (sortdatafield != null && sortorder != "")
            {
                featuredGames = Utils.Utils.SortOrders<view_ADM_FeaturedGame>(featuredGames, sortdatafield, sortorder);
            }
            var result = new
            {
                TotalRows = total,
                Rows = featuredGames
            };
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult UpdateFeaturedGames(GetFeaturedGamesResult result)
        {
            context.UpdateFeaturedGame(result.Id.ToString(),
                                result.Game,
                                result.GameMd5,
                                Convert.ToBoolean(result.Approved),
                                Convert.ToBoolean(result.Visible),
                                Convert.ToBoolean(result.Banner),
                                result.Warning,
                                Convert.ToBoolean(result.Analytics),
                                result.Order,
                                result.CompanyOrder
            );

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}