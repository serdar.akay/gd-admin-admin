import { mandatory } from '../modules/mandatory';
import { extendDefaults } from '../modules/extendDefaults';

class DropBox {

    constructor(element = mandatory(), options) {

        // Set defaults, which could be overwritten.
        var defaults = {
            list: 'fileList'
        };

        // Create options by extending defaults with the passed in arguments.
        if (options) {
            this.options = extendDefaults(defaults, options);
        } else {
            this.options = defaults;
        }

        // Get our needed elements.
        this.targetContainer = element || false;
        this.fileElem = this.targetContainer.querySelector('input[type=file]') || false;
        this.fileList = this.targetContainer.getElementsByClassName(this.options.list)[0] || false;
        this.button = this.targetContainer.querySelector('button[type=submit]') || false;

        this.windowURL = window.URL || window.webkitURL;

        // Check for missing elements.
        if (!this.targetContainer || !this.fileElem || !this.fileList || !this.button) {
            throw new Error('Missing a dropbox element!');
        }
    }

    create() {
        this.targetContainer.addEventListener('dragenter', this.dragenter, false);
        this.targetContainer.addEventListener('dragover', this.dragover, false);
        this.targetContainer.addEventListener('drop', this.drop.bind(this), false);

        this.fileElem.addEventListener('change', (e) => {
            this.handleFiles(e.target.files);
            e.preventDefault();
        }, false);
    }

    dragover(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    dragenter(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    drop(e) {
        e.stopPropagation();
        e.preventDefault();

        var dt = e.dataTransfer;
        var files = dt.files;

        this.handleFiles(files);
    }

    handleFiles(files) {
        if (!files.length) {
            this.fileList.innerHTML = '<p>No files selected!</p>';
            this.button.style.display = 'none';
        } else {
            console.info(files);
            this.fileList.innerHTML = "";
            var list = document.createElement('ul');
            this.fileList.appendChild(list);
            for (var i = 0; i < files.length; i++) {
                var li = document.createElement('li');
                list.appendChild(li);

                var img = document.createElement('img');
                img.src = this.windowURL.createObjectURL(files[i]);
                img.height = 60;
                img.onload = () => {
                    this.windowURL.revokeObjectURL(this.src);
                };
                li.appendChild(img);

                var container = document.createElement('span');
                li.appendChild(container);

                var name = document.createElement('span');
                name.innerHTML = files[i].name;
                container.appendChild(name);

                var size = document.createElement('span');
                size.innerHTML = files[i].size + ' bytes';
                container.appendChild(size);
            }

            this.button.style.display = 'block';
        }
    }
}

export default DropBox;





