let instance = null;

import { scrollAnimator } from '../modules/scrollAnimator';

class Page {

    static get instance() {
        return instance;
    }

    constructor() {

        // Make this a singleton.
        if (instance) {
            return instance;
        } else {
            instance = this;
        }

        //Add some classes to the <head> which we can hook into for CSS theme purposes.
        var today = new Date();
        var month = today.getMonth() + 1;
        var html = document.getElementsByTagName('html')[0];
        var day = today.getDate();
        html.className = 'y' + today.getFullYear();
        if (month < 10) {
            html.className += ' m0' + month;
        } else {
            html.className += ' m' + month;
        }
        if (day < 10) {
            html.className += ' d0' + day;
        } else {
            html.className += ' d' + day;
        }

        //Add some classes to the <head> which we can hook into for cross browser purposes.
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            html.className += ' IE' + parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            html.className += ' IE' + parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            html.className += ' IE' + parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        return instance;
    }

    enableAnchorAnimations() {
        var anchors = document.getElementsByClassName('anchor');
        for (var i = 0; i < anchors.length; i++) {
            var href = anchors[i].getAttribute('href');
            anchors[i].addEventListener('click', function() {
                scrollAnimator(0, href, 1000);
            });
        }
    }
}

export default Page;
