import Page from './pages/page';
import HomePage from './pages/homepage';
import GamePage from './pages/gamepage';
import AddGamePage from './pages/addgamepage';

/**
 * Load components for specific page types.
 * It's required to add a page-type to the <body> tag, matching with the cases inside the switch statement.
 */
var documentClass = document.getElementsByTagName('body') || false;
var documentType = documentClass[0].getAttribute('page-type') || false;
if (documentType) {
    switch (documentType) {
        case 'homepage':
            var homePage = new HomePage();
            homePage.enableAnchorAnimations();
            homePage.enableCarousel();
            break;
        case 'gamepage':
            var gamePage = new GamePage();
            gamePage.enableAnchorAnimations();
            gamePage.enableGame();
            gamePage.enableVooxe();
            gamePage.enableClipboards();
            break;
        case 'addgamepage':
            var addGamePage = new AddGamePage();
            addGamePage.enableDropBox();
            break;
        default:
            var page = new Page();
            page.enableAnchorAnimations();
    }
} else {
    throw new Error('Missing page-type value!');
}
