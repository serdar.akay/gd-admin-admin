﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GameDistribution.Startup))]
namespace GameDistribution
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
