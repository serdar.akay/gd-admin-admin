import Page from '../pages/page';
import DropBox from '../components/dropbox';

let instance = null;

class AddGamePage extends Page {

    static get instance() {
        return instance;
    }

    constructor() {
        super();

        // Make this a singleton.
        if (instance) {
            return instance;
        } else {
            instance = this;
        }

        return instance;
    }

    enableDropBox() {
        var screenshotBoxElement = document.getElementById('box-screenshot');
        var screenshotBox = new DropBox(screenshotBoxElement, {list: 'list'});
        screenshotBox.create();

        var iconsBoxElement = document.getElementById('box-icons');
        var iconsBox = new DropBox(iconsBoxElement, {list: 'list'});
        iconsBox.create();
    }
}

export default AddGamePage;