﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Linq;

namespace GameDistribution.Utils
{
    public class Utils
    {
        public const string REGISTER_EMAIL = "gd@GameDistribution.com";
        private static string _fromEmail = "gamedistribution@vooxe.com";

        public const string GAMES_FILEPATH = "D:\\Hosts\\games.flashgamesubmitter.com\\";
        public const string IMG_FILEPATH = "D:\\Hosts\\img.flashgamesubmitter.com\\";
        public const string SWF_FILEPATH = "D:\\Hosts\\swf.flashgamesubmitter.com\\";

        //public const string IMG_FILEPATH = "D:\\VStudioC#\\GameDistribution\\trunk\\web\\swf\\";
        //public const string SWF_FILEPATH = "D:\\VStudioC#\\GameDistribution\\trunk\\web\\swf\\";

        //public const string GAMES_FILEPATH = "C:\\VStudioC#\\";
        //public const string IMG_FILEPATH = "C:\\VStudioC#\\";
        //public const string SWF_FILEPATH = "C:\\VStudioC#\\";

        //public const string UNITY_FILEPATH = "D:\\VStudioC#\\GameDistribution\\trunk\\web\\swf\\";
        //public const string APK_FILEPATH = "D:\\VStudioC#\\GameDistribution\\trunk\\web\\apk\\";
        //public const string IOS_FILEPATH = "D:\\VStudioC#\\GameDistribution\\trunk\\web\\swf\\";
        //public const string HTML5_FILEPATH = "D:\\VStudioC#\\GameDistribution\\trunk\\web\\html5\\";

        public const string UNITY_FILEPATH = "C:\\VStudioC#\\";
        public const string APK_FILEPATH = "C:\\VStudioC#\\";
        public const string IOS_FILEPATH = "C:\\VStudioC#\\";
        public const string HTML5_FILEPATH = "C:\\VStudioC#\\";

        public string FromEmail
        {
            get { return _fromEmail; }
            set { _fromEmail = value; }
        }

        private static string _fromDisplayName = "GameDistribution";
        public string FromDisplayName
        {
            get { return _fromDisplayName; }
            set { _fromDisplayName = value; }
        }
        private static string _LoginPassword = "reha2016report";
        public string LoginPassword
        {
            get { return _LoginPassword; }
            set { _LoginPassword = value; }
        }
        public static string safeStr(object val)
        {
            return safeStr(val, "");
        }
        public static string safeStr(object val, string def)
        {
            try
            {
                return Convert.ToString(val);

            }
            catch
            {
                return def;
            }
        }

        public static int safeInt(object val, int def)
        {
            try
            {
                return Convert.ToInt32(val);

            }
            catch
            {
                return def;
            }
        }

        public static int safeInt(object val)
        {
            return safeInt(val, 0);
        }

        public static bool sendEmail(string From, string To, string Subject, string Body)
        {
            try
            {
                MailMessage mail = new MailMessage(From, To);
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("gd@gamedistribution.com", "rehaerol2012");
                client.Host = "23.81.66.190";
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                client.Timeout = 10000;
                client.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool gmailEmailServer(string toMail, string fullName, string subject, string htmlBody)
        {
            var fromAddress = new MailAddress(_fromEmail, _fromDisplayName);
            var toAddress = new MailAddress(toMail, fullName);

            try
            {
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, _LoginPassword)
                };
                //using (var message = new MailMessage(fromAddress, toAddress)
                //{
                //    Subject = subject,
                //    Body = htmlBody,
                //    IsBodyHtml = true,
                //    BodyTransferEncoding = System.Net.Mime.TransferEncoding.Base64
                //})
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = htmlBody,
                    IsBodyHtml = true
                })
                {
                    //message.Attachments.Add(new System.Net.Mail.Attachment(fileStream, fileName));
                    smtp.Send(message);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DataSet getCachedSPDS(string procname, object[,] parameters, bool useCache = true)
        {
            DataSet result = null;
            string key = "";

            if (useCache)
            {
                key = procname + "_";

                for (int i = 0; i < parameters.GetLength(0); i++)
                    key += safeStr(parameters[i, 1]) + "_";

                key = md5(key);

                result = getCachedDS(key);
                if (result != null)
                    return result;
            }
            return result;
        }

        public static DataSet getCachedDS(string key)
        {
            DataSet ret = (DataSet)HttpContext.Current.Cache[key];
            return ret;
        }
        public static string md5(string value)
        {
            MD5CryptoServiceProvider _md5 = new MD5CryptoServiceProvider();

            byte[] btr = Encoding.UTF8.GetBytes(value);
            btr = _md5.ComputeHash(btr);

            StringBuilder sb = new StringBuilder();

            foreach (byte ba in btr)
            {
                sb.Append(ba.ToString("x2").ToLower());
            }

            return sb.ToString();
        }

        public static bool validateEmail(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static string _GET(string key)
        {
            return HttpContext.Current.Request.QueryString[key];
        }

        public static void WriteToFile(string strPath, byte[] Buffer)
        {
            FileStream newFile = new FileStream(strPath, FileMode.Create);
            newFile.Write(Buffer, 0, Buffer.Length);
            newFile.Close();
        }

        public static bool uploadFiles(HttpPostedFileBase fileCollection, string savePath, string saveName)
        {
            HttpPostedFile MyFile;
            int FileLen;
            System.IO.Stream MyStream;

            try
            {
                //for (int i = 0; i < fileCollection.Count; i++)
                //{
                //    MyFile = fileCollection[i];

                FileLen = fileCollection.ContentLength;
                if (FileLen > 0)
                {
                    byte[] input = new byte[FileLen];

                    MyStream = fileCollection.InputStream;
                    MyStream.Read(input, 0, FileLen);

                    string fileExtention = Path.GetExtension(fileCollection.FileName).ToLower();

                    if (fileExtention == ".swf" || fileExtention == ".unity3d" || fileExtention == ".html5" || fileExtention == ".apk" || fileExtention == ".ipa" || fileExtention == ".jpg" || fileExtention == ".png")
                    {
                        WriteToFile(savePath + saveName + fileExtention, input);
                    }
                    else
                    {
                        return false;
                    }

                }
                // }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public enum GameTypes { NONE, FLASH, UNITY, ANDROID, IOS, HTML5 };

        public static string calcFileMd5(string filename)
        {
            byte[] fileMD5;

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    fileMD5 = md5.ComputeHash(stream);
                }
            }

            return BitConverter.ToString(fileMD5).Replace("-", "").ToLower();
        }

        public const string UNITY_URL = "http://swf.GameDistribution.com/";
        public const string APK_URL = "http://apk.GameDistribution.com/";
        public const string IPA_URL = "http://ipa.GameDistribution.com/";
        public const string HTML5_URL = "http://html5.GameDistribution.com/";

        public const string IMG_URL = "http://img.GameDistribution.com/";
        public const string SWF_URL = "http://swf.GameDistribution.com/";




        public static string BuildQuery(System.Collections.Specialized.NameValueCollection query, string tsql)
        {
            var filtersCount = int.Parse(query.GetValues("filterscount")[0]);
            var queryString = tsql;
            var tmpDataField = "";
            var tmpFilterOperator = "";
            var where = "";
            if (filtersCount > 0)
            {
                where = " WHERE (";
            }
            for (var i = 0; i < filtersCount; i += 1)
            {
                var filterValue = query.GetValues("filtervalue" + i)[0];
                var filterCondition = query.GetValues("filtercondition" + i)[0];
                var filterDataField = query.GetValues("filterdatafield" + i)[0];
                var filterOperator = query.GetValues("filteroperator" + i)[0];
                if (tmpDataField == "")
                {
                    tmpDataField = filterDataField;
                }
                else if (tmpDataField != filterDataField)
                {
                    where += ") AND (";
                }
                else if (tmpDataField == filterDataField)
                {
                    if (tmpFilterOperator == "")
                    {
                        where += " AND ";
                    }
                    else
                    {
                        where += " OR ";
                    }
                }

                where += GetFilterCondition(filterCondition, filterDataField, filterValue);
                if (filterDataField == "Game")
                {
                    where += " OR GameMd5 LIKE '%" + filterValue + "%'";
                }
                if (i == filtersCount - 1)
                {
                    where += ")";
                }
                tmpFilterOperator = filterOperator;
                tmpDataField = filterDataField;
            }
            queryString += where;
            return queryString;
        }

        private static string GetFilterCondition(string filterCondition, string filterDataField, string filterValue)
        {
            if (filterDataField == "AddedOn")
            {
                filterCondition = "BETWEEN";
            }

            switch (filterCondition)
            {
                case "NOT_EMPTY":
                case "NOT_NULL":
                    return " " + filterDataField + " NOT LIKE '" + "" + "'";
                case "EMPTY":
                case "NULL":
                    return " " + filterDataField + " LIKE '" + "" + "'";
                case "CONTAINS_CASE_SENSITIVE":
                    return " " + filterDataField + " LIKE '%" + filterValue + "%'" + " COLLATE SQL_Latin1_General_CP1_CS_AS";
                case "CONTAINS":
                    return " " + filterDataField + " LIKE '%" + filterValue + "%'";
                case "DOES_NOT_CONTAIN_CASE_SENSITIVE":
                    return " " + filterDataField + " NOT LIKE '%" + filterValue + "%'" + " COLLATE SQL_Latin1_General_CP1_CS_AS"; ;
                case "DOES_NOT_CONTAIN":
                    return " " + filterDataField + " NOT LIKE '%" + filterValue + "%'";
                case "EQUAL_CASE_SENSITIVE":
                    return " " + filterDataField + " = '" + filterValue + "'" + " COLLATE SQL_Latin1_General_CP1_CS_AS"; ;
                case "EQUAL":
                    return " " + filterDataField + " = '" + filterValue + "'";
                case "NOT_EQUAL_CASE_SENSITIVE":
                    return " BINARY " + filterDataField + " <> '" + filterValue + "'";
                case "NOT_EQUAL":
                    return " " + filterDataField + " <> '" + filterValue + "'";
                case "GREATER_THAN":
                    return " " + filterDataField + " > '" + filterValue + "'";
                case "LESS_THAN":
                    return " " + filterDataField + " < '" + filterValue + "'";
                case "GREATER_THAN_OR_EQUAL":
                    return " " + filterDataField + " >= '" + filterValue + "'";
                case "LESS_THAN_OR_EQUAL":
                    return " " + filterDataField + " <= '" + filterValue + "'";
                case "STARTS_WITH_CASE_SENSITIVE":
                    return " " + filterDataField + " LIKE '" + filterValue + "%'" + " COLLATE SQL_Latin1_General_CP1_CS_AS"; ;
                case "STARTS_WITH":
                    return " " + filterDataField + " LIKE '" + filterValue + "%'";
                case "ENDS_WITH_CASE_SENSITIVE":
                    return " " + filterDataField + " LIKE '%" + filterValue + "'" + " COLLATE SQL_Latin1_General_CP1_CS_AS"; ;
                case "ENDS_WITH":
                    return " " + filterDataField + " LIKE '%" + filterValue + "'";
                case "BETWEEN":
                    DateTime startDate = Convert.ToDateTime(filterValue);
                    startDate = Convert.ToDateTime(filterValue);
                    DateTime endDate = startDate.AddDays(1);
                    string start = startDate.ToString("yyyy-MM-dd");
                    string end = endDate.ToString("yyyy-MM-dd");
                    return " " + filterDataField + " >= '" + start + "' AND " + filterDataField + "<'" + end + "'";
            }
            return "";
        }

        public static IEnumerable<T> SortOrders<T>(IEnumerable<T> collection, string sortField, string sortOrder)
        {
            if (sortOrder == "asc")
            {
                collection = collection.OrderBy(c => c.GetType().GetProperty(sortField).GetValue(c, null));
            }
            else
            {
                collection = collection.OrderByDescending(c => c.GetType().GetProperty(sortField).GetValue(c, null));
            }
            return collection;
        }


        public static string TrimDomain(string domain)
        {
            var words = new string[] { "www.", "https://wwww.", "http://wwww." };

            foreach (var item in words)
            {
                if (domain.Contains(item))
                {
                    domain = domain.Replace(item, "");
                    break;
                }
            }

            //domain = domain.Split(".".ToArray())[0];

            return domain.Trim();
        }
    }
}