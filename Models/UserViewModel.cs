﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GameDistribution.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Firstname is required")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Lastname is required")]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        public bool? Active { get; set; }

        [Required(ErrorMessage = "Company is required")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Website is required")]
        public string Website { get; set; }

        public DateTime RegDate { get; set; }

        public int? userType { get; set; }
        public string userTypeId { get; set; }

        public string PartnerId { get; set; }

        public string DeveloperId { get; set; }

        public string UserId { get; set; }

        public string RegId { get; set; }

        public bool SendEmail { get; set; }

        //Advertisemnt
        public bool? BannerEnable { get; set; }

        public bool? PreRoll { get; set; }

        public int? ShowAfterTime { get; set; }
    }

}