﻿namespace GameDistribution.Models
{
    public class DomainViewModel
    {
        public int Id { get; set; }

        public string GameId { get; set; }

        public string Title { get; set; }

        public string Domain { get; set; }

        public bool? BannerEnable { get; set; }

        public bool? PreRoll { get; set; }

        public int? ShowAfterTime { get; set; }
        
    }
}