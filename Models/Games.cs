﻿using System;

namespace GameDistribution.Models
{
    public class Games
    {
        public string CatTitle { get; set; }
        public string Description { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ActivedDate { get; set; }
    }
}