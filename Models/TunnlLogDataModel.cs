﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameDistribution.Models
{
    public class TunnlLogDataModel
    {
        public string ResponseCode { get; set; }
        public string ResponseText { get; set; }
        public TunnlReport Data { get; set; }
        public bool IsValid { get; set; }
    }
    public class TunnlReport
    {
        public int? Id { get; set; }
        public int? Number { get; set; }
    }
}