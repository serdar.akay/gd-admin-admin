﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace GameDistribution.Models
{
    public class IdentityModel
    {
        // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
        public class ApplicationUser : IdentityUser
        {
            public string Name { get; set; }
            public string Lastname { get; set; }
            public string CompanyName { get; set; }
            public string WebSiteURL { get; set; }
            public string RawPassword { get; set; }

            public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
            {
                // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
                var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
                // Add custom user claims here
                userIdentity.AddClaim(new Claim("CompanyName", this.CompanyName == null ? "" : this.CompanyName));
                userIdentity.AddClaim(new Claim("Name", this.Name == null ? "" : this.Name));
                userIdentity.AddClaim(new Claim("Lastname", this.Lastname == null ? "" : this.Lastname));
                userIdentity.AddClaim(new Claim("WebSiteURL", this.WebSiteURL == null ? "" : this.WebSiteURL));
                userIdentity.AddClaim(new Claim("RawPassword", this.RawPassword == null ? "" : this.RawPassword));
                return userIdentity;
            }
        }

        public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
        {
            public ApplicationDbContext()
                : base("GameDistributionConnectionString", throwIfV1Schema: false)
            {
            }

            public static ApplicationDbContext Create()
            {
                return new ApplicationDbContext();
            }
        }

        public class IdentityManager
        {
            public bool RoleExists(string name)
            {
                var rm = new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(new ApplicationDbContext()));
                return rm.RoleExists(name);
            }

            public bool CreateRole(string name)
            {
                var rm = new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(new ApplicationDbContext()));
                var idResult = rm.Create(new IdentityRole(name));
                return idResult.Succeeded;
            }

            public bool CreateUser(ApplicationUser user, string password)
            {
                var um = new UserManager<ApplicationUser>(
                    new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var idResult = um.Create(user, password);
                return idResult.Succeeded;
            }

            public bool AddUserToRole(string userId, string roleName)
            {
                var um = new UserManager<ApplicationUser>(
                    new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var idResult = um.AddToRole(userId, roleName);
                return idResult.Succeeded;
            }

            public void ClearUserRoles(string userId)
            {
                var um = new UserManager<ApplicationUser>(
                    new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var user = um.FindById(userId);
                var currentRoles = new List<IdentityUserRole>();
                currentRoles.AddRange(user.Roles);
                foreach (var role in currentRoles)
                {
                    um.RemoveFromRole(userId, role.RoleId);
                }
            }
        }
    }
}