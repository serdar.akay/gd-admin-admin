﻿using System.ComponentModel.DataAnnotations;

namespace GameDistribution.Models
{
    public class LoginViewModel
    {
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}